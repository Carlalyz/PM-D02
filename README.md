# 线上评估报告

---
* [产品原型链接](http://nfunm172018125.gitee.io/app_team/#id=p9vxcw&p=1_1加载页_广告页&g=1)
* [原产品gitee文档链接](https://gitee.com/NFUNM172018125/APP_team#%E6%9C%89%E5%BD%B1app%E6%A6%82%E8%BF%B0)
* 原产品项目作者：柯根、林泽伟、王鹏杰、李铮、黄舒静、张柳燕
---

##### 当前小组
* 周五班8-10 D02组
* 小组成员：李一鸣、林莉明、麦安琪、丁晓莹、陈立豪
* [用户旅程地图链接](https://www.processon.com/view/link/5efeee67f346fb1ae58fbce9)  ---  [服务蓝图链接](https://images.gitee.com/uploads/images/2020/0715/232944_14a37f12_2228569.jpeg "服务蓝图.jpg")  ---  [三页方案小组汇总](https://gitee.com/Carlalyz/PM-D02/blob/master/%E4%B8%89%E9%A1%B5%E6%96%B9%E6%A1%88.md) --- [三页方案共享文档](https://www.processon.com/view/link/5f002c3c7d9c08442049263c)
---
### 用户旅程地图 [链接](https://www.processon.com/view/link/5efeee67f346fb1ae58fbce9)
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/190652_7d8803a9_2228569.jpeg "用户旅程地图.jpg")

### 服务蓝图 [链接](https://www.processon.com/view/link/5eff12d8e0b34d4dba6905b4)
![输入图片说明](https://images.gitee.com/uploads/images/2020/0715/232944_14a37f12_2228569.jpeg "服务蓝图.jpg")

---

### 1.ESG问题、ESG机会罗列
* 关于ESG社会问题/机会
  * 会出现在用户发现和获取该APP的时候，容易涉及人权问题 / 在企业中营造平等和谐的氛围，接受包容用户的差异和多样性
  * 会出现在用户发布电影图解的时候，容易涉及发布内容是否带有不良用词的问题 / 发布图解前，后台进行敏感词检查
  * 会出现在用户获取影视资讯的时候，电影图文解说的版权来源问题 / 与图解的原创者充分沟通，添加原创标识，标注好图解来源
  * 会出现在用户进行分享（为我们二次传播）的时候，APP植入流氓广告 / 加大内容审查力度，屏蔽疑似广告的关键词

* 关于ESG环境问题/机会
  * 会出现在用户购买相关周边的时候，制作的周边原材料是否健康环保 / 统一供应平台，公开制作信息符合环保标准

* 关于ESG治理问题/机会
  * 会出现在用户成功下载并使用APP后，APP后台限权滥用的问题 / 在用户须知中明确app权限范围，给用户可选择的空间
  * 会出现在用户使用APP的推荐分区和搜索部分，推荐内容是否健康和搜索内容质量问题 / 设置未成年人保护机制
  * 会出现在用户进行线下观影的时候，隐含商业竞争不公平的现象 / 管理透明、重视审查刷分营销等乱象

### 2.以上问题/机会在用户旅程地图中的位置
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/191009_bd18835c_2228569.png "户旅程地图.png")
***

## 3.市场调研A

### 同领域产品：
* 图解电影 [参考链接](http://www.2265.com/soft/65909.html) [百度百科](https://baike.baidu.com/item/%E5%9B%BE%E8%A7%A3%E7%94%B5%E5%BD%B1/8545095?fr=aladdin)
* 有影 [参考链接](http://www.pc6.com/az/611269.html)
* 十分钟电影 [参考链接](http://www.downcc.com/soft/45044.html) [百度百科](https://baike.baidu.com/item/%E5%8D%81%E5%88%86%E9%92%9F%E7%94%B5%E5%BD%B1/16536725?fr=aladdin)

### 分析描述：
#### 同类型产品：图解电影 [（相关链接1：图解电影app点评）](https://www.jianshu.com/p/9570a9bd381f)  [（相关链接2：图解电影APP体验报告）](https://www.jianshu.com/p/a71d53825a60)
![介绍](https://images.gitee.com/uploads/images/2020/0703/010640_fbfd929f_2230210.png "图解电影介绍.png")
* **面向群体：**  图解电影的内容主要由社区里活跃的图解师（图解内容作者）创作产生，用户在观看图解的过程中，可以通过弹幕、评论、打赏等方式，参与到内容的阅读和互动中来。
* **上线时目标用户：**  a)积极的图解爱好者；b)电影爱好者
* **需求满足：**  等公交，搭地铁，午休前，睡前，等候排队时，图解电影可以填充用户各种零碎的时间。
* **ESG治理方面：**
    * 1、抗风险性。封面都是电影截图，app本身也内置了很多电影的截图，图片这一块的抗风险较好
![图解电影1](https://images.gitee.com/uploads/images/2020/0703/001601_a3429133_2230210.png "图解电影1.png")
    * 2、图解内容质量方面。把资源集中，丰富片型，明确分类，五个主要功能结构：我的，推荐，分类，片场搜索，让用户更快的发现图解内容，更加规范专业的平台和奖励机制也给图解作者一个更好的创作环境，促进优质图解的产生。

* **ESG社会问题：（负面报道）**
    * 图解电影发生过侵权问题，优酷将“图解电影”告上法庭。2019年8月6日，法院一审判决，认定“图解电影”侵权，需赔偿优酷经济损失3万元。图解电影在版权管理问题上仍然存在不足，“图解电影”平台上的图解涵盖了涉案剧集的主要画面和情节，侵害了著作权归属公司的信息网络传播权。[点击查看相关链接：全国首例图解电影案宣判：被告赔偿优酷3万元](http://news.sina.com.cn/sf/news/ajjj/2019-08-07/doc-ihytcerm9073050.shtml) 
![侵权](https://images.gitee.com/uploads/images/2020/0703/005649_2c26c8ce_2230210.png "侵权.png")

***

## 4.市场调研B

1.  在今天像短视频这类快速获取信息的软件有很大市场，用户并没有那么多时间去看一场需要两个多小时的电影，他们更期望能利用碎片化的时间来娱乐。图片+文字这种模式简单但也容易面临侵权等问题，图解电影与影视媒体、线下影院和发行公司多方合作可以为APP本身盈利不高，有版权风险的情况下寻找新方向。
* 相关链接[十分钟读懂一部电影，图解电影如何玩转“电影快餐”生意](https://www.jiemian.com/article/1274937.html)
2.  图解电影市场发展潜力大，据统计，2016年全国电影总票房为457.12亿元，同比增长3.73%；城市院线观影人次为13.72亿，同比增长8.89%，但由于功能比较垂直单一，属于小众需求，用户的依赖程度较小。从百度指数的数据统计，和电影关键字的搜索相比，图解电影几乎可以忽略不计。
3. 从线下电影来看，周边衍生品市场需求巨大，据公开数据显示，在好莱坞的电影收入构成中，衍生品收入占总体收入七成。图解电影结合衍生品开发可以辅助电影宣传，吸引流量，提高产品的商业价值
* 相关链接[图解电影产品体验报告：互联网电影市场的另类，应该何去何从？](http://www.woshipm.com/evaluating/687275.html)

***

### 5.分析总结

**认为当前产品可以进行上线**
#### 原因：
+ 电影市场现在总体来说呈现一个良好的发展趋势，电影的类型越来越多样，越来越贴近人民群众的生活，同时也在影响着人民群众的生活，而在移动市场中主推电影内容类的产品不多。
+ 相对于“图解电影”贴吧的火爆以及网上类似的公众号、微博大V等，市场上关于专门图解电影的app仍然比较少。
+ 电影票务系统及周边衍生品市场需求巨大，而我们的app正好满足需求。
+ 总的来说，图解电影很好的利用了电影庞大市场中用户确存在的图解电影的需求这一细分垂直领域，虽然该产品市场容量不大，但是需求存在已久，并不是一个伪命题。同时商业拓展性较强，前景尚可。

#### 后期迭代注意：

+ 吸引该领域KOL(知名度高，号召力强，高活跃的用户)的入驻。
+ 应用运营者更加注重版权。

